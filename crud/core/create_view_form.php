<?php

$string = '

<div style="margin-top: -30px;">
    <h2><?php echo $title ?></h2>
    <?php echo create_breadcrumb($home, $home_link); ?>    
</div>


<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-list"></i> <?php echo $sub_title; ?> </h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body">
      <?php echo form_open($action); ?>
        <table class="table table-bordered">
';

foreach ($non_pk as $row) {
    if ($row["data_type"] == 'text') {
$string .= "
          <tr>
            <td>". label($row["column_name"]) . " <?php echo form_error('" . $row["column_name"] . "') ?></td>
            <td><textarea class=\"form-control\" rows=\"3\" name=\"" . $row["column_name"] . "\" id=\"" . $row["column_name"] . "\" placeholder=\"" . label($row["column_name"]) . "\"><?php echo $" . $row["column_name"] . "; ?></textarea></td>
          </tr>";
    } else {
        $string .= "
          <tr>
            <td>". label($row["column_name"]) . " <?php echo form_error('" . $row["column_name"] . "') ?></td>
            <td><input type=\"text\" class=\"form-control\" name=\"" . $row["column_name"] . "\" id=\"" . $row["column_name"] . "\" placeholder=\"" . label($row["column_name"]) . "\" value=\"<?php echo $" . $row["column_name"] . "; ?>\" /></td>
          </tr>";
    }
}
$string .= "
          <tr>
            <td colspan='2'>
              <button type=\"submit\" class=\"btn btn-primary\"><?php echo \$button ?></button>
              <a href=\"<?php echo site_url(\$module.'/" . $c_url . "') ?>\" class=\"btn btn-default\">Cancel</a>
            </td>
          </tr>
        </table>";
$string .= "
          <input type=\"hidden\" name=\"" . $pk . "\" value=\"<?php echo $" . $pk . "; ?>\" /> ";

$string .= '
      <?php form_close(); ?>
    </div>
</div>';


$hasil_view_form = createFile($string, $target."views/" . $c_url . "/" . $v_form_file);

?>