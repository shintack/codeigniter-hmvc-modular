<?php 

$string = '
<div style="margin-top: -30px;">
    <h2><?php echo $title ?></h2>
    <?php echo create_breadcrumb($home, $home_link); ?>    
</div>


<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-list"></i> <?php echo $sub_title; ?> </h3>
        <div class="box-tools pull-right">
            <a href="<?php echo site_url($module."/'.$c_url.'/create") ?>">
                <button type="button" class="btn btn-sm btn-primary" > <i class="fa fa-plus"></i> Add</button>
            </a>
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body">
        <table class="table table-bordered">';
foreach ($non_pk as $row) {
$string .= "
          <tr><td>".label($row["column_name"])."</td><td><?php echo $".$row["column_name"]."; ?></td></tr>";
}
$string .= '
          <tr><td></td><td><a href="<?php echo site_url($module."/'.$c_url.'") ?>" class="btn btn-default">Cancel</a></td></tr>
        </table>
    </div>
</div>
';

$hasil_view_read = createFile($string, $target."views/" . $c_url . "/" . $v_read_file);

?>