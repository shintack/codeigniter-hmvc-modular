<?php

$export_pdf     = '1';
$export_word    = '1';
$export_excel   = '1';

$string = "<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class " . $c . " extends CI_Controller
{    
    var \$_mod; 
    var \$_tabel;
    var \$_pk;
    var \$_field;
    var \$_template;

    function __construct()
    {
        parent::__construct();
        \$this->load->model('$m');
        \$this->_init();
    }";

$string .= "\n
    //1. _init
    public function _init()
    {
        \$this->_mod    = '';//" . $c . "'; 
        \$this->_tabel  = '$table_name';
        \$this->_pk     = '$pk';
        \$this->_field  = [ "; 
foreach ($non_pk as $row) {
$string .= "'" . $row['column_name'] ."',";
}  
$string .= " ];
        \$this->_template = 'template';
    }
";

if ($jenis_tabel == 'reguler_table') {
    
$string .= "\n\n
    /**
     * buat liat data dimari tabel & satuan 
     * -- content --
     * 1. index         => list data
     * 2. read          => liat data satuan
     * 3. create        => form polosan 
     * 4. update        => form isian 
     */
    //1. index list data
    public function index()
    {
        \$q = urldecode(\$this->input->get('q', TRUE));
        \$v = urldecode(\$this->input->get('v', TRUE));
        \$start = intval(\$this->input->get('start'));
        \$limit = (is_numeric(\$v)>0)?\$v:10;

        if ((\$q || \$v) <> '') {
            \$config['base_url'] = base_url() . '$c_url/index.html?' . 'v=' . urlencode(\$v) . '&q=' . urlencode(\$q);
            \$config['first_url'] = base_url() . '$c_url/index.html?' . 'v=' . urlencode(\$v) . '&q=' . urlencode(\$q);;
        }else {
            \$config['base_url'] = base_url() . '$c_url/index.html';
            \$config['first_url'] = base_url() . '$c_url/index.html';
        }

        \$config['per_page'] = \$limit;
        \$config['page_query_string'] = TRUE;
        \$config['total_rows'] = \$this->" . $m . "->get_limit_data(\$this->_tabel,\$this->_pk,0, \$start, \$q, \$this->_field );
        \$$c_url = \$this->" . $m . "->get_limit_data(\$this->_tabel,\$this->_pk,\$limit, \$start, \$q, \$this->_field );
        \$this->load->library('pagination');
        \$this->pagination->initialize(\$config);

        \$data = array(
            '" . $c_url . "_data' => \$$c_url,
            'q' => \$q,'v' => \$v,
            'pagination' => \$this->pagination->create_links(),
            'total_rows' => \$config['total_rows'],
            'start' => \$start,
            'input' => \$this->" . $m . "->_set_value(),
        );
        //add data breadcumbs
        \$data['title'] = '" . $c . "';
        \$data['sub_title'] = 'Daftar Data';
        \$data['home']  = 'Dashboard';
        \$data['home_link'] = site_url(); 
        //set module
        \$data['module'] = \$this->_mod;
        \$this->template->load(\$this->_template,\$this->_mod.'/$c_url/$v_list',\$data);
    }";

} else {
    
$string .="\n\n    public function index()
    {
        \$$c_url = \$this->" . $m . "->get_all();

        \$data = array(
            '" . $c_url . "_data' => \$$c_url
        );
        \$this->template->load(\$this->_template,\$this->_mod.'/$c_url/$v_list',\$data);
    }";

}
    
$string .= "\n
    //2. liat data satuan
    public function read(\$id) 
    {
        \$where = [ \$this->_pk => \$id ];
        \$row = \$this->" . $m . "->get_row(\$this->_tabel,\$where);
        if (\$row) {
            \$data = \$this->" . $m ."->_set_value(\$row);
            //add data breadcumbs
            \$data['title'] = '" . $c . "';
            \$data['sub_title'] = 'Lihat Data';
            \$data['home']  = 'Dashboard';
            \$data['home_link'] = site_url(); 
            //set module
            \$data['module'] = \$this->_mod;
            \$this->template->load(\$this->_template,\$this->_mod.'/$c_url/$v_read',\$data);
        } else {
            \$this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url(\$this->_mod.'/$c_url'));
        }
    }

    //3. create 
    public function create() 
    {
        \$data = \$this->" . $m . "->_set_value();
        //add data breadcumbs
        \$data['title'] = '" . $c . "';
        \$data['sub_title'] = 'Tambah Data';
        \$data['home']  = 'Dashboard';
        \$data['home_link'] = site_url(); 
        //set module
        \$data['module'] = \$this->_mod;
        \$this->template->load(\$this->_template,\$this->_mod.'/$c_url/$v_form',\$data);
    }

    //4. update
    public function update(\$id) 
    {
        \$where = [ \$this->_pk => \$id ];
        \$row = \$this->" . $m . "->get_row(\$this->_tabel,\$where);
        if (\$row) {
            \$data = \$this->" . $m . "->_set_value(\$row);
            //add data breadcumbs
            \$data['title'] = '" . $c . "';
            \$data['sub_title'] = 'Update Data';
            \$data['home']  = 'Dashboard';
            \$data['home_link'] = site_url(); 
            //set module
            \$data['module'] = \$this->_mod;
            \$this->template->load(\$this->_template,\$this->_mod.'/$c_url/$v_form',\$data);
        } else {
            \$this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url(\$this->_mod.'/$c_url'));
        }
    }


    /**
     * dimari khusus buat proses data ya bos 
     * -- content --
     * 1. create_action => save data
     * 2. update_action => update data
     * 3. delete        => hapus data 
     * 4. excel         => export to excel 
     */
    public function create_action() 
    {
        \$this->" . $m . "->_rules();

        if (\$this->form_validation->run() == FALSE) {
            \$this->create();
        } else {
            \$data = \$this->" . $m . "->_get_post();

            \$this->" . $m . "->insert(\$this->_tabel,\$data);
            \$this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url(\$this->_mod.'/$c_url'));
        }
    }

    public function update_action() 
    {
        \$this->" . $m . "->_rules();

        if (\$this->form_validation->run() == FALSE) {
            \$this->update(\$this->input->post('$pk', TRUE));
        } else {
            \$data = \$this->" . $m . "->_get_post();
            \$this->" . $m . "->update(\$this->_tabel,\$this->_pk,\$this->input->post('$pk', TRUE), \$data);
            \$this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url(\$this->_mod.'/$c_url'));
        }
    }
 
    public function delete() 
    {
        \$id    = \$this->input->post(\$this->_pk,TRUE);
        \$where = [ \$this->_pk => \$id ];
        \$row   = \$this->" . $m . "->get_row(\$this->_tabel,\$where);
        if (\$row) {
            \$this->".$m."->delete(\$this->_tabel,\$this->_pk,\$id);
            \$this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url(\$this->_mod.'/$c_url'));
        } else {
            \$this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url(\$this->_mod.'/$c_url'));
        }
    }";

if ($export_excel == '1') {
    $string .= "\n\n
    public function excel()
    {
        \$this->load->helper('exportexcel');
        \$namaFile = \"$table_name.xls\";
        \$judul = \"$table_name\";
        \$tablehead = 0;
        \$tablebody = 1;
        \$nourut = 1;
        //penulisan header
        header(\"Pragma: public\");
        header(\"Expires: 0\");
        header(\"Cache-Control: must-revalidate, post-check=0,pre-check=0\");
        header(\"Content-Type: application/force-download\");
        header(\"Content-Type: application/octet-stream\");
        header(\"Content-Type: application/download\");
        header(\"Content-Disposition: attachment;filename=\" . \$namaFile . \"\");
        header(\"Content-Transfer-Encoding: binary \");

        xlsBOF();

        \$kolomhead = 0;
        xlsWriteLabel(\$tablehead, \$kolomhead++, \"No\");";
foreach ($non_pk as $row) {
        $column_name = label($row['column_name']);
        $string .= "\n\t\txlsWriteLabel(\$tablehead, \$kolomhead++, \"$column_name\");";
}
$string .= "\n\n\t\tforeach (\$this->" . $m . "->get_all(\$this->_tabel,\$this->_pk) as \$data) {
            \$kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber(\$tablebody, \$kolombody++, \$nourut);";
foreach ($non_pk as $row) {
        $column_name = $row['column_name'];
        $xlsWrite = $row['data_type'] == 'int' || $row['data_type'] == 'double' || $row['data_type'] == 'decimal' ? 'xlsWriteNumber' : 'xlsWriteLabel';
        $string .= "\n\t\t\t" . $xlsWrite . "(\$tablebody, \$kolombody++, \$data->$column_name);";
}
$string .= "\n\n\t\t\t\$tablebody++;
            \$nourut++;
        }

        xlsEOF();
        exit();
    }";
}

if ($export_word == '1') {
    $string .= "\n\n    public function word()
    {
        header(\"Content-type: application/vnd.ms-word\");
        header(\"Content-Disposition: attachment;Filename=$table_name.doc\");

        \$data = array(
            '" . $table_name . "_data' => \$this->" . $m . "->get_all(\$this->_tabel,\$this->_pk),
            'start' => 0
        );
        
        \$this->load->view(\$this->_mod.'/$c_url/" . $v_doc . "',\$data);
    }";
}

if ($export_pdf == '1') {
    $string .= "\n\n    function pdf()
    {
        \$data = array(
            '" . $table_name . "_data' => \$this->" . $m . "->get_all(\$this->_tabel,\$this->_pk),
            'start' => 0
        );
        
        \$this->load->library('Pdf');
        \$html = \$this->pdf->load_view(\$this->_mod.'/". $table_name ."/". $v_pdf . "', \$data);
        \$this->pdf->render();
        \$filename = 'pdf';
        \$this->pdf->stream(\$filename, array('Attachment' => false));
        exit(0); 
    }";
}

$string .= "\n\n}\n\n/* End of file $c_file */
/* Location: ./application/controllers/$c_file */
/* Please DO NOT modify this information : */
/* Generated by shintackeror ".date('Y-m-d H:i:s')." */
/* http://shintackeror.web.id thanks to http://harviacode.com */";


$hasil_controller = createFile($string, $target . "controllers/" . $c_file);

?>