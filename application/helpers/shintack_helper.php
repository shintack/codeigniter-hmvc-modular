<?php
function cmb_dinamis($name,$table,$field,$pk,$selected){
    $ci = get_instance();
    $cmb = "<select name='$name' class='form-control'>";
    $data = $ci->db->get($table)->result();
    foreach ($data as $d){
        $cmb .="<option value='".$d->$pk."'";
        $cmb .= $selected==$d->$pk?" selected='selected'":'';
        $cmb .=">".  strtoupper($d->$field)."</option>";
    }
    $cmb .="</select>";
    return $cmb;  
}

if (!function_exists('logged_in')) {
    function logged_in(){
        $ci = &get_instance();
        $ci->load->library(array('ion_auth'));
        if (!$ci->ion_auth->logged_in())
        {
            redirect('auth/login', 'refresh');
        }
    }
}

if (!function_exists('create_breadcrumb')) {
    function create_breadcrumb($initial_crumb = '', $initial_crumb_url = '') {
        $ci = &get_instance();
        $open_tag = '<ol class="breadcrumb">';
        $close_tag = '</ol>';
        $crumb_open_tag = '<li>';
        $active_crumb_open_tag = '<li class="active">';
        $crumb_close_tag = '</li>';
        $separator = '<span class="crumb-separator"></span>';
        $total_segments = $ci->uri->total_segments();
        $breadcrumbs = $open_tag;
        if ($initial_crumb != '') {
            $breadcrumbs .= $crumb_open_tag;
            $breadcrumbs .= create_crumb_href($initial_crumb, false, true) . $separator;
        }
        
        $segment = '';
        $crumb_href = '';
        
        for ($i = 1; $i <= $total_segments; $i++) {
            
            $segment = $ci->uri->segment($i);
            $crumb_href .= $ci->uri->segment($i) . '/';
            
            if ($total_segments > $i) {
                $breadcrumbs .= $crumb_open_tag;
                $breadcrumbs .= create_crumb_href($segment, $crumb_href);
                $breadcrumbs .= $separator;
            }else{
                $breadcrumbs .= $active_crumb_open_tag;
                $breadcrumbs .= create_crumb_href($segment, $crumb_href);
            }
            
            $breadcrumbs .= $crumb_close_tag;
        }
        $breadcrumbs .= $close_tag;
        return $breadcrumbs;
    }
}
if (!function_exists('create_crumb_href')) {
    function create_crumb_href($uri_segment, $crumb_href = false, $initial = false) {
        $ci = &get_instance();
        $base_url = $ci->config->base_url();
        
        $crumb_href = rtrim($crumb_href, '/');
        
        if($initial) {
            return '<a href="' . $base_url . '">' . ucwords(str_replace(array('-', '_'), ' ', $uri_segment)) . '</a>';
        }else{
            return '<a href="' . $base_url . $crumb_href . '">' . ucwords(str_replace(array('-', '_'), ' ', $uri_segment)) . '</a>';
        }
    }
}

if (!function_exists('vardump')) {
    function vardump($data) {
        echo '<pre>';
        var_dump($data);
        echo '</pre>';exit();
    }
}

