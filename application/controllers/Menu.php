<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Menu extends CI_Controller
{    
    var $_mod; 
    var $_tabel;
    var $_pk;
    var $_field;
    var $_template;

    function __construct()
    {
        parent::__construct();
        $this->load->model('Menu_model');
        $this->_init();
        logged_in();
    }

    //1. _init
    public function _init()
    {
        $this->_mod    = '';//Menu'; 
        $this->_tabel  = 'menu';
        $this->_pk     = 'id';
        $this->_field  = [ 'name','link','icon','is_active','is_parent', ];
        $this->_template = 'template';
    }



    /**
     * buat liat data dimari tabel & satuan 
     * -- content --
     * 1. index         => list data
     * 2. read          => liat data satuan
     * 3. create        => form polosan 
     * 4. update        => form isian 
     */
    //1. index list data
    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $v = urldecode($this->input->get('v', TRUE));
        $start = intval($this->input->get('start'));
        $limit = (is_numeric($v)>0)?$v:10;

        if (($q || $v) <> '') {
            $config['base_url'] = base_url() . 'menu/index.html?' . 'v=' . urlencode($v) . '&q=' . urlencode($q);
            $config['first_url'] = base_url() . 'menu/index.html?' . 'v=' . urlencode($v) . '&q=' . urlencode($q);;
        }else {
            $config['base_url'] = base_url() . 'menu/index.html';
            $config['first_url'] = base_url() . 'menu/index.html';
        }

        $config['per_page'] = $limit;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Menu_model->get_limit_data($this->_tabel,$this->_pk,0, $start, $q, $this->_field );
        $menu = $this->Menu_model->get_limit_data($this->_tabel,$this->_pk,$limit, $start, $q, $this->_field );
        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'menu_data' => $menu,
            'q' => $q,'v' => $v,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'input' => $this->Menu_model->_set_value(),
        );
        //add data breadcumbs
        $data['title'] = 'Menu';
        $data['sub_title'] = 'Daftar Data';
        $data['home']  = 'Dashboard';
        $data['home_link'] = site_url(); 
        //set module
        $data['module'] = $this->_mod;
        $this->template->load($this->_template,$this->_mod.'/menu/menu_list',$data);
    }

    //2. liat data satuan
    public function read($id) 
    {
        $where = [ $this->_pk => $id ];
        $row = $this->Menu_model->get_row($this->_tabel,$where);
        if ($row) {
            $data = $this->Menu_model->_set_value($row);
            //add data breadcumbs
            $data['title'] = 'Menu';
            $data['sub_title'] = 'Lihat Data';
            $data['home']  = 'Dashboard';
            $data['home_link'] = site_url(); 
            //set module
            $data['module'] = $this->_mod;
            $this->template->load($this->_template,$this->_mod.'/menu/menu_read',$data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url($this->_mod.'/menu'));
        }
    }

    //3. create 
    public function create() 
    {
        $data = $this->Menu_model->_set_value();
        //add data breadcumbs
        $data['title'] = 'Menu';
        $data['sub_title'] = 'Tambah Data';
        $data['home']  = 'Dashboard';
        $data['home_link'] = site_url(); 
        //set module
        $data['module'] = $this->_mod;
        $this->template->load($this->_template,$this->_mod.'/menu/menu_form',$data);
    }

    //4. update
    public function update($id) 
    {
        $where = [ $this->_pk => $id ];
        $row = $this->Menu_model->get_row($this->_tabel,$where);
        if ($row) {
            $data = $this->Menu_model->_set_value($row);
            //add data breadcumbs
            $data['title'] = 'Menu';
            $data['sub_title'] = 'Update Data';
            $data['home']  = 'Dashboard';
            $data['home_link'] = site_url(); 
            //set module
            $data['module'] = $this->_mod;
            $this->template->load($this->_template,$this->_mod.'/menu/menu_form',$data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url($this->_mod.'/menu'));
        }
    }


    /**
     * dimari khusus buat proses data ya bos 
     * -- content --
     * 1. create_action => save data
     * 2. update_action => update data
     * 3. delete        => hapus data 
     * 4. excel         => export to excel 
     */
    public function create_action() 
    {
        $this->Menu_model->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = $this->Menu_model->_get_post();

            $this->Menu_model->insert($this->_tabel,$data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url($this->_mod.'/menu'));
        }
    }

    public function update_action() 
    {
        $this->Menu_model->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = $this->Menu_model->_get_post();
            $this->Menu_model->update($this->_tabel,$this->_pk,$this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url($this->_mod.'/menu'));
        }
    }
 
    public function delete() 
    {
        $id    = $this->input->post($this->_pk,TRUE);
        $where = [ $this->_pk => $id ];
        $row   = $this->Menu_model->get_row($this->_tabel,$where);
        if ($row) {
            $this->Menu_model->delete($this->_tabel,$this->_pk,$id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url($this->_mod.'/menu'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url($this->_mod.'/menu'));
        }
    }


    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "menu.xls";
        $judul = "menu";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
		xlsWriteLabel($tablehead, $kolomhead++, "Name");
		xlsWriteLabel($tablehead, $kolomhead++, "Link");
		xlsWriteLabel($tablehead, $kolomhead++, "Icon");
		xlsWriteLabel($tablehead, $kolomhead++, "Is Active");
		xlsWriteLabel($tablehead, $kolomhead++, "Is Parent");

		foreach ($this->Menu_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
			xlsWriteLabel($tablebody, $kolombody++, $data->name);
			xlsWriteLabel($tablebody, $kolombody++, $data->link);
			xlsWriteLabel($tablebody, $kolombody++, $data->icon);
			xlsWriteNumber($tablebody, $kolombody++, $data->is_active);
			xlsWriteNumber($tablebody, $kolombody++, $data->is_parent);

			$tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=menu.doc");

        $data = array(
            'menu_data' => $this->Menu_model->get_all(),
            'start' => 0
        );
        
        $this->load->view($this->_mod.'/menu/menu_doc',$data);
    }

    function pdf()
    {
        $data = array(
            'menu_data' => $this->Menu_model->get_all(),
            'start' => 0
        );
        
        ini_set('memory_limit', '32M');
        $html = $this->load->view($this->_mod.'/menu/menu_pdf', $data, true);
        $this->load->library('pdf');
        $pdf = $this->pdf->load();
        $pdf->WriteHTML($html);
        $pdf->Output('menu/menu.pdf', 'D'); 
    }

}

/* End of file Menu.php */
/* Location: ./application/controllers/Menu.php */
/* Please DO NOT modify this information : */
/* Generated by shintackeror 2018-03-11 13:56:03 */
/* http://shintackeror.web.id thanks to http://harviacode.com */