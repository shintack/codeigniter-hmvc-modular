<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Login_attempts extends CI_Controller
{    
    var $_mod; 
    var $_tabel;
    var $_pk;
    var $_field;
    var $_template;

    function __construct()
    {
        parent::__construct();
        $this->load->model('Login_attempts_model');
        $this->_init();
    }

    //1. _init
    public function _init()
    {
        $this->_mod    = '';//Login_attempts'; 
        $this->_tabel  = 'login_attempts';
        $this->_pk     = 'id';
        $this->_field  = [ 'ip_address','login','time', ];
        $this->_template = 'template';
    }



    /**
     * buat liat data dimari tabel & satuan 
     * -- content --
     * 1. index         => list data
     * 2. read          => liat data satuan
     * 3. create        => form polosan 
     * 4. update        => form isian 
     */
    //1. index list data
    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $v = urldecode($this->input->get('v', TRUE));
        $start = intval($this->input->get('start'));
        $limit = (is_numeric($v)>0)?$v:10;

        if (($q || $v) <> '') {
            $config['base_url'] = base_url() . 'login_attempts/index.html?' . 'v=' . urlencode($v) . '&q=' . urlencode($q);
            $config['first_url'] = base_url() . 'login_attempts/index.html?' . 'v=' . urlencode($v) . '&q=' . urlencode($q);;
        }else {
            $config['base_url'] = base_url() . 'login_attempts/index.html';
            $config['first_url'] = base_url() . 'login_attempts/index.html';
        }

        $config['per_page'] = $limit;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Login_attempts_model->get_limit_data($this->_tabel,$this->_pk,0, $start, $q, $this->_field );
        $login_attempts = $this->Login_attempts_model->get_limit_data($this->_tabel,$this->_pk,$limit, $start, $q, $this->_field );
        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'login_attempts_data' => $login_attempts,
            'q' => $q,'v' => $v,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'input' => $this->Login_attempts_model->_set_value(),
        );
        //add data breadcumbs
        $data['title'] = 'Login_attempts';
        $data['sub_title'] = 'Daftar Data';
        $data['home']  = 'Dashboard';
        $data['home_link'] = site_url(); 
        //set module
        $data['module'] = $this->_mod;
        $this->template->load($this->_template,$this->_mod.'/login_attempts/login_attempts_list',$data);
    }

    //2. liat data satuan
    public function read($id) 
    {
        $where = [ $this->_pk => $id ];
        $row = $this->Login_attempts_model->get_row($this->_tabel,$where);
        if ($row) {
            $data = $this->Login_attempts_model->_set_value($row);
            //add data breadcumbs
            $data['title'] = 'Login_attempts';
            $data['sub_title'] = 'Lihat Data';
            $data['home']  = 'Dashboard';
            $data['home_link'] = site_url(); 
            //set module
            $data['module'] = $this->_mod;
            $this->template->load($this->_template,$this->_mod.'/login_attempts/login_attempts_read',$data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url($this->_mod.'/login_attempts'));
        }
    }

    //3. create 
    public function create() 
    {
        $data = $this->Login_attempts_model->_set_value();
        //add data breadcumbs
        $data['title'] = 'Login_attempts';
        $data['sub_title'] = 'Tambah Data';
        $data['home']  = 'Dashboard';
        $data['home_link'] = site_url(); 
        //set module
        $data['module'] = $this->_mod;
        $this->template->load($this->_template,$this->_mod.'/login_attempts/login_attempts_form',$data);
    }

    //4. update
    public function update($id) 
    {
        $where = [ $this->_pk => $id ];
        $row = $this->Login_attempts_model->get_row($this->_tabel,$where);
        if ($row) {
            $data = $this->Login_attempts_model->_set_value($row);
            //add data breadcumbs
            $data['title'] = 'Login_attempts';
            $data['sub_title'] = 'Update Data';
            $data['home']  = 'Dashboard';
            $data['home_link'] = site_url(); 
            //set module
            $data['module'] = $this->_mod;
            $this->template->load($this->_template,$this->_mod.'/login_attempts/login_attempts_form',$data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url($this->_mod.'/login_attempts'));
        }
    }


    /**
     * dimari khusus buat proses data ya bos 
     * -- content --
     * 1. create_action => save data
     * 2. update_action => update data
     * 3. delete        => hapus data 
     * 4. excel         => export to excel 
     */
    public function create_action() 
    {
        $this->Login_attempts_model->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = $this->Login_attempts_model->_get_post();

            $this->Login_attempts_model->insert($this->_tabel,$data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url($this->_mod.'/login_attempts'));
        }
    }

    public function update_action() 
    {
        $this->Login_attempts_model->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = $this->Login_attempts_model->_get_post();
            $this->Login_attempts_model->update($this->_tabel,$this->_pk,$this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url($this->_mod.'/login_attempts'));
        }
    }
 
    public function delete() 
    {
        $id    = $this->input->post($this->_pk,TRUE);
        $where = [ $this->_pk => $id ];
        $row   = $this->Login_attempts_model->get_row($this->_tabel,$where);
        if ($row) {
            $this->Login_attempts_model->delete($this->_tabel,$this->_pk,$id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url($this->_mod.'/login_attempts'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url($this->_mod.'/login_attempts'));
        }
    }


    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "login_attempts.xls";
        $judul = "login_attempts";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
		xlsWriteLabel($tablehead, $kolomhead++, "Ip Address");
		xlsWriteLabel($tablehead, $kolomhead++, "Login");
		xlsWriteLabel($tablehead, $kolomhead++, "Time");

		foreach ($this->Login_attempts_model->get_all($this->_tabel,$this->_pk) as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
			xlsWriteLabel($tablebody, $kolombody++, $data->ip_address);
			xlsWriteLabel($tablebody, $kolombody++, $data->login);
			xlsWriteNumber($tablebody, $kolombody++, $data->time);

			$tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=login_attempts.doc");

        $data = array(
            'login_attempts_data' => $this->Login_attempts_model->get_all($this->_tabel,$this->_pk),
            'start' => 0
        );
        
        $this->load->view($this->_mod.'/login_attempts/login_attempts_doc',$data);
    }

    function pdf()
    {
        $data = array(
            'login_attempts_data' => $this->Login_attempts_model->get_all($this->_tabel,$this->_pk),
            'start' => 0
        );
        
        $this->load->library('Pdf');
        $html = $this->pdf->load_view($this->_mod.'/login_attempts/login_attempts_pdf', $data);
        $this->pdf->render();
        $filename = 'pdf';
        $this->pdf->stream($filename, array('Attachment' => false));
        exit(0); 
    }

}

/* End of file Login_attempts.php */
/* Location: ./application/controllers/Login_attempts.php */
/* Please DO NOT modify this information : */
/* Generated by shintackeror 2018-03-11 17:03:03 */
/* http://shintackeror.web.id thanks to http://harviacode.com */