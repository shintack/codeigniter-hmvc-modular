<?php (defined('BASEPATH')) OR exit('No direct script access allowed');


class MY_Model extends CI_Model 
{
	public function __construct()
    {
        parent::__construct();
	}
	
	/**
	 * get one ror
	 * @param string $tabel nama tabel
	 * @param string $field_order 
	 * @param string $order
	 * @return array object
	 */
    function get_all($tabel,$field_order,$order='DESC')
    {
        $this->db->order_by($field_order, $order);
        return $this->db->get($tabel)->result();
    }

	/**
	 * get one ror
	 * @param string $tabel nama tabel
	 * @param array $where 
	 * @return array object
	 */
	function get_row($tabel,$where)
	{
		if ($tabel!==NULL) {
			if (is_array($where)) {
				foreach($where as $key => $value){
					$this->db->where($key,$value);
				}
			}
			return $this->db->get($tabel)->row();
		}
    }
    
    function get_where_result($tabel,$where)
	{
		if ($tabel!==NULL) {
			if (is_array($where)) {
				foreach($where as $key => $value){
					$this->db->where($key,$value);
				}
			}
			return $this->db->get($tabel)->result();
		}
    }
    

	// get data with limit and search
    function get_limit_data($tabel,$pk,$limit = 0, $start = 0, $q = NULL, $fields = NULL, $order = 'DESC', $join = NULL, $type_join = NULL) {
        $this->db->order_by($pk, $order);
        if (is_array($fields)) {
            foreach($fields as $key => $value){
                if ($key[0]) {
                    $this->db->like($value, $q);              
                }
                $this->db->or_like($value, $q);
            }
        }
        if (is_array($join)) {
            foreach($join as $tb_join => $field){
                if ($type_join != NULL) {
                   // $this->db->join($tabel,$field,$type_join);
                }else{
                    $this->db->join($tb_join,$field);
                }
            }
        }
        if (is_numeric($limit)&&$limit>0) {
            return $this->db->limit($limit, $start)->get($tabel)->result();
        }else{
            return $this->db->get($tabel)->num_rows();
        }
    }

    // insert data
    function insert($tabel,$data)
    {
        $this->db->insert($tabel, $data);
        return $this->db->insert_id();	
    }

    // update data
    function update($tabel,$pk,$id, $data)
    {
        $this->db->where($pk, $id);
        return $this->db->update($tabel, $data);
    }

    // delete data
    function delete($tabel,$pk,$id)
    {
        $this->db->where($pk, $id);
        return $this->db->delete($tabel);
    }

}