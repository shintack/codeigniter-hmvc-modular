

<div style="margin-top: -30px;">
    <h2><?php echo $title ?></h2>
    <?php echo create_breadcrumb($home, $home_link); ?>    
</div>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-list"></i> <?php echo $sub_title; ?> </h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModalAdd"> <i class="fa fa-plus"></i> Add</button>
            <?php /*
            <a href="<?php echo site_url($module."/menu/create") ?>">
                <button type="button" class="btn btn-sm btn-primary" > <i class="fa fa-plus"></i> Add</button>
            </a>
            */ ?>
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body">
        <form action="<?php echo site_url($module."/menu/index"); ?>" class="form-inline" method="get">
             <div class="row" style="margin-bottom: 10px;">
                <div class="col-md-3 text-left">                
                    <div class="input-group">
                        <select name="v" class="form-control">
                            <option value="10" <?=($v==10)?"selected":"";?>>10</option>
                            <option value="25" <?=($v==25)?"selected":"";?>>25</option>
                            <option value="50" <?=($v==50)?"selected":"";?>>50</option>
                            <option value="100" <?=($v==100)?"selected":"";?>>100</option>
                        </select>
                        <span class="input-group-btn">
                            <?php if ($v <> ""):?>
                                <a href="<?php echo site_url($module."/menu"); ?>" class="btn btn-default"><i class="fa fa-refresh"></i></a>
                            <?php endif; ?>
                            <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i></button>
                        </span>
                    </div>
                    
                </div><!-- show data -->
                <div class="col-md-5 text-center">
                    <div style="margin-top: 8px" id="message">
                        <?php echo $this->session->userdata("message") <> "" ? $this->session->userdata("message") : ""; ?>
                    </div>
                </div><!-- message -->
                <div class="col-md-1 text-right"></div>
                <div class="col-md-3 text-right">
                    
                    <div class="input-group">
                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                        <span class="input-group-btn">
                            <?php if ($q <> ""):?>
                                <a href="<?php echo site_url($module."/menu"); ?>" class="btn btn-default"><i class="fa fa-refresh"></i></a>
                            <?php endif; ?>
                          <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                    
                </div><!-- search -->          
            </div>
        </form>

        <!-- tabel here -->

        <div style="overflow: auto; margin-bottom: 10px;">
            <table class="table table-bordered table-striped table-hover" style="margin-bottom: 10px">
                <thead class="btn-primary">
                    <tr>
                        <th align="center" width="10px">No</th>
                        <th>Name</th>
                        <th>Link</th>
                        <th>Icon</th>
                        <th>Is Active</th>
                        <th>Is Parent</th>            
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($menu_data as $menu):?>
                    <tr>
                        <td width="10px"><?php echo ++$start ?></td>
                        <td><?php echo $menu->name ?></td>
                        <td><?php echo $menu->link ?></td>
                        <td><?php echo $menu->icon ?></td>
                        <td><?php echo $menu->is_active ?></td>
                        <td><?php echo $menu->is_parent ?></td>
                        <td style="text-align:center" width="140px">
                        <?php 
                        echo anchor(site_url($module.'/menu/read/'.$menu->id),'<i class="fa fa-eye"></i>',array('title'=>'detail','class'=>'btn btn-info btn-sm'));
                        echo '  ';
                        echo anchor(site_url($module.'/menu/update/'.$menu->id),'<i class="fa fa-pencil-square-o"></i>',array('title'=>'edit','class'=>'btn btn-warning btn-sm'));
                        echo '  ';
                        //echo anchor(site_url($module.'/menu/delete/'.$menu->id),'<i class="fa fa-trash-o"></i>','title="delete" class="btn btn-danger btn-sm" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); ?>
                        <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#myModalDelete<?=$menu->id?>"><i class="fa fa-trash-o"></i></button>
                        </td>
                    </tr>

                    <!-- Modal -->
                    <div id="myModalDelete<?=$menu->id?>" class="modal fade" role="dialog">
                      <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header bg-red">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Peringatan!!!</h4>
                          </div>
                          <div class="modal-body">
                            <h1>Yakin akan menghapus data ini? </h1>
                          </div>
                          <div class="modal-footer">
                            <?=form_open($input["delete"])?>
                            <input type="hidden" name="id" value="<?=$menu->id?>" />
                            <button type="submit" class="btn btn-danger">Delete</button>
                            <?=form_close()?>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                          </div>
                        </div>

                      </div>
                    </div>

                <?php endforeach; ?>

                </tbody>
            </table>
        </div>
        <!-- ./tabel here -->

        <div class="row">
            <div class="col-md-6">
                <a href="#" class="btn btn-primary btn-sm">Total Record : <?php echo $total_rows ?></a>
                <?php echo anchor(site_url($module.'/menu/excel'), '<i class="fa fa-file-excel-o"></i>', 'title="Export to Excel" class="btn btn-success btn-sm"'); ?>
                <?php echo anchor(site_url($module.'/menu/word'), '<i class="fa fa-file-word-o"></i>', 'title="Export to Word"class="btn btn-info btn-sm"'); ?>
                <?php echo anchor(site_url($module.'/menu/pdf'), '<i class="fa fa-file-pdf-o"></i>', 'title="Export to Pdf"class="btn btn-danger btn-sm"'); ?>
            </div>
            <div class="col-md-6 text-right">
                <?php echo $pagination ?>
            </div>
        </div>

    </div>
</div>


<!-- Modal -->
<div id="myModalAdd" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
<?php echo form_open($input["action"]); ?>
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add <?=$title?></h4>
      </div>
      <div class="modal-body">
        <table class="table table-bordered">

          <tr>
            <td>Name <?php echo form_error('name') ?></td>
            <td><input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $input['name']; ?>" /></td>
          </tr>
          <tr>
            <td>Link <?php echo form_error('link') ?></td>
            <td><input type="text" class="form-control" name="link" id="link" placeholder="Link" value="<?php echo $input['link']; ?>" /></td>
          </tr>
          <tr>
            <td>Icon <?php echo form_error('icon') ?></td>
            <td><input type="text" class="form-control" name="icon" id="icon" placeholder="Icon" value="<?php echo $input['icon']; ?>" /></td>
          </tr>
          <tr>
            <td>Is Active <?php echo form_error('is_active') ?></td>
            <td><input type="text" class="form-control" name="is_active" id="is_active" placeholder="Is Active" value="<?php echo $input['is_active']; ?>" /></td>
          </tr>
          <tr>
            <td>Is Parent <?php echo form_error('is_parent') ?></td>
            <td><input type="text" class="form-control" name="is_parent" id="is_parent" placeholder="Is Parent" value="<?php echo $input['is_parent']; ?>" /></td>
          </tr>
        </table>
        <input type="hidden" name="id" value="<?php echo $input['id']; ?>" /> 
      </div>
      <div class="modal-footer">
            <button type="submit" class="btn btn-primary"><?php echo $input["button"] ?></button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>
<?php form_close(); ?>

  </div>
</div>


