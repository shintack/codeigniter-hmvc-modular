

<div style="margin-top: -30px;">
    <h2><?php echo $title ?></h2>
    <?php echo create_breadcrumb($home, $home_link); ?>    
</div>


<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-list"></i> <?php echo $sub_title; ?> </h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body">
      <?php echo form_open($action); ?>
        <table class="table table-bordered">

          <tr>
            <td>Name <?php echo form_error('name') ?></td>
            <td><input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $name; ?>" /></td>
          </tr>
          <tr>
            <td>Link <?php echo form_error('link') ?></td>
            <td><input type="text" class="form-control" name="link" id="link" placeholder="Link" value="<?php echo $link; ?>" /></td>
          </tr>
          <tr>
            <td>Icon <?php echo form_error('icon') ?></td>
            <td><input type="text" class="form-control" name="icon" id="icon" placeholder="Icon" value="<?php echo $icon; ?>" /></td>
          </tr>
          <tr>
            <td>Is Active <?php echo form_error('is_active') ?></td>
            <td><input type="text" class="form-control" name="is_active" id="is_active" placeholder="Is Active" value="<?php echo $is_active; ?>" /></td>
          </tr>
          <tr>
            <td>Is Parent <?php echo form_error('is_parent') ?></td>
            <td><input type="text" class="form-control" name="is_parent" id="is_parent" placeholder="Is Parent" value="<?php echo $is_parent; ?>" /></td>
          </tr>
          <tr>
            <td colspan='2'>
              <button type="submit" class="btn btn-primary"><?php echo $button ?></button>
              <a href="<?php echo site_url($module.'/menu') ?>" class="btn btn-default">Cancel</a>
            </td>
          </tr>
        </table>
          <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
      <?php form_close(); ?>
    </div>
</div>