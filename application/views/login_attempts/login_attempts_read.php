
<div style="margin-top: -30px;">
    <h2><?php echo $title ?></h2>
    <?php echo create_breadcrumb($home, $home_link); ?>    
</div>


<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-list"></i> <?php echo $sub_title; ?> </h3>
        <div class="box-tools pull-right">
            <a href="<?php echo site_url($module."/login_attempts/create") ?>">
                <button type="button" class="btn btn-sm btn-primary" > <i class="fa fa-plus"></i> Add</button>
            </a>
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body">
        <table class="table table-bordered">
          <tr><td>Ip Address</td><td><?php echo $ip_address; ?></td></tr>
          <tr><td>Login</td><td><?php echo $login; ?></td></tr>
          <tr><td>Time</td><td><?php echo $time; ?></td></tr>
          <tr><td></td><td><a href="<?php echo site_url($module."/login_attempts") ?>" class="btn btn-default">Cancel</a></td></tr>
        </table>
    </div>
</div>
