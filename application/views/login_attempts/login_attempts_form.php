

<div style="margin-top: -30px;">
    <h2><?php echo $title ?></h2>
    <?php echo create_breadcrumb($home, $home_link); ?>    
</div>


<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-list"></i> <?php echo $sub_title; ?> </h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body">
      <?php echo form_open($action); ?>
        <table class="table table-bordered">

          <tr>
            <td>Ip Address <?php echo form_error('ip_address') ?></td>
            <td><input type="text" class="form-control" name="ip_address" id="ip_address" placeholder="Ip Address" value="<?php echo $ip_address; ?>" /></td>
          </tr>
          <tr>
            <td>Login <?php echo form_error('login') ?></td>
            <td><input type="text" class="form-control" name="login" id="login" placeholder="Login" value="<?php echo $login; ?>" /></td>
          </tr>
          <tr>
            <td>Time <?php echo form_error('time') ?></td>
            <td><input type="text" class="form-control" name="time" id="time" placeholder="Time" value="<?php echo $time; ?>" /></td>
          </tr>
          <tr>
            <td colspan='2'>
              <button type="submit" class="btn btn-primary"><?php echo $button ?></button>
              <a href="<?php echo site_url($module.'/login_attempts') ?>" class="btn btn-default">Cancel</a>
            </td>
          </tr>
        </table>
          <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
      <?php form_close(); ?>
    </div>
</div>